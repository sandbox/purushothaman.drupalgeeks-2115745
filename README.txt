Commerce Checkout Register
===============

Description
-----------

Commerce Checkout Register module provides user registration functionality 
for Drupal Commerce on checkout page.
(http://drupal.org/project/commerce).

Module extend the feature of Commerce Checkout Login where it provides an inline 
registration form via AJAX. If the e-mail address, when an anonymous user enters 
is not associated with any user account, then the checkout form will expand 
with a username, password textfield and sign up button for immediate 
registration and login.

Sponsored by DrupalGeeks.org (http://www.drupalgeeks.org/).

Dependencies
------------

Drupal Commerce and all of its dependencies
Commerce Checkout Login


Installation
------------

Normal installation just like other modules. Install module on site/all
/contrib/module/ folder and enable it.
